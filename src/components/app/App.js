import React, { useState } from 'react'

import items from 'data/items'

import './App.scss'

const removeItemByIndex = (items, index) => {
	items.splice(index, 1)

	return items
}

const App = () => {
	const [firstOptions, setFirstOptions] = useState([...items])
	const [firstSelected, setFirstSelected] = useState([])
	const [secondOptions, setSecondOptions] = useState([...items])
	const [secondSelected, setSecondSelected] = useState([])

	const limit = 4
	const firstPlaceHolder = Array(limit - firstSelected.length).fill()
	const secondPlaceHolder = Array(limit - secondSelected.length).fill()

	const handleFirstSelectChange = event => {
		const index = event.target.value
		const selected = firstOptions[index]

		setFirstSelected([selected, ...firstSelected])
		setFirstOptions(removeItemByIndex(firstOptions, index))
		setSecondOptions(secondOptions.filter(({ id }) => id !== selected.id))
	}

	const handleFirstRemoveSelected = index => () => {
		const item = firstSelected[index]
		setFirstOptions([item, ...firstOptions])
		setSecondOptions([item, ...secondOptions])

		setFirstSelected(removeItemByIndex(firstSelected, index))
	}

	const handleSecondSelectChange = event => {
		const index = event.target.value
		const selected = secondOptions[index]

		setSecondSelected([secondOptions[index], ...secondSelected])
		setSecondOptions(removeItemByIndex(secondOptions, index))
		setFirstOptions(firstOptions.filter(({ id }) => id !== selected.id))
	}

	const handleSecondRemoveSelected = index => () => {
		const item = firstSelected[index]
		setSecondOptions([item, ...secondOptions])
		setFirstOptions([item, ...firstOptions])

		setSecondSelected(removeItemByIndex(secondSelected, index))
	}

	return (
		<div className="container">
			<h2>Select four options from the first dropdown:</h2>

			<div class="select-container">
				<select
					onChange={handleFirstSelectChange}
					disabled={firstSelected.length === limit}
				>
					{firstOptions.map(({ id, name }, index) => (
						<option key={id} value={index}>
							{name}
						</option>
					))}
				</select>

				{firstSelected.map(({ id, name }, index) => (
					<div className="selected" key={id}>
						{name}
						<button onClick={handleFirstRemoveSelected(index)}>&times;</button>
					</div>
				))}

				{firstPlaceHolder.map(() => (
					<div className="placeholder" />
				))}
			</div>

			<h2>Select four options from the second dropdown:</h2>

			<div class="select-container">
				<select
					onChange={handleSecondSelectChange}
					disabled={secondSelected.length === limit}
				>
					{secondOptions.map(({ id, name }, index) => (
						<option key={id} value={index}>
							{name}
						</option>
					))}
				</select>

				{secondSelected.map(({ id, name }, index) => (
					<div className="selected" key={id}>
						{name}
						<button onClick={handleSecondRemoveSelected(index)}>&times;</button>
					</div>
				))}

				{secondPlaceHolder.map(() => (
					<div className="placeholder" />
				))}
			</div>
		</div>
	)
}

export default App
